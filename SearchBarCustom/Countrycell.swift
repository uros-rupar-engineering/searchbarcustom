//
//  Countrycell.swift
//  SearchBarCustom
//
//  Created by uros.rupar on 6/4/21.
//

import UIKit

class Countrycell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var countryLabel: UILabel!
}
