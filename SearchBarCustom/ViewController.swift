//
//  ViewController.swift
//  SearchBarCustom
//
//  Created by uros.rupar on 6/4/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchResultsUpdating{
  
    
    

    

    
    
    
    var data  = [String]()
    
    var filteredData = [String]()
    
    var shouldShowSearchResults = false
    
    
    @IBOutlet weak var mytable: UITableView!
    
    var searchController: UISearchController!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if shouldShowSearchResults {
            return filteredData.count
        }
        else {
            return data.count
        }
       
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Countrycell
        
       
        
        if shouldShowSearchResults {
            cell.countryLabel.text = filteredData[indexPath.row]
        }else{
            cell.countryLabel.text = data[indexPath.row]
        }
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
         data = CountryData.loadListofCountries()
   
        
        configureSearchController()
        
        
    }

   
 
    func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.placeholder = "Search here..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        mytable.tableHeaderView = searchController.searchBar
    }
    

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        shouldShowSearchResults = true
        mytable.reloadData()
    }
     
     
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        mytable.reloadData()
    }
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            mytable.reloadData()
        }
     
        searchController.searchBar.resignFirstResponder()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text
        
        filteredData = data.filter({ (country) -> Bool in
            let countryText: NSString = country as NSString

            return (countryText.range(of:searchString!,options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        mytable.reloadData()
    }

    


    
   
    

    
}

