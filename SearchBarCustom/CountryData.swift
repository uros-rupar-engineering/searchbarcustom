//
//  CountryData.swift
//  SearchBarCustom
//
//  Created by uros.rupar on 6/4/21.
//

import Foundation

class CountryData{
    
    
    
    static func loadListofCountries() -> [String]{
        
        
        var data :[String] = []
        let fileName = "countries"
        let fileXstension = "txt"
        
        var fileContent : String?
    
        let path = Bundle.main.path(forResource: fileName, ofType: fileXstension)
        
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        
        if let fileContent = fileContent{
            
           
            
            let lineByLineArray = fileContent.split(separator: "\n")
            
            
            for row in lineByLineArray{
                
                let country = String(row)
                
                data.append(country)
            }
        
    }
        
        return data
}
}
